package main

import (
	"encoding/binary"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
)

func initCIDRMap(cidrMap map[int]string) {
	cidrMap[8] = "255.0.0.0"
	cidrMap[9] = "255.128.0.0"
	cidrMap[10] = "255.192.0.0"
	cidrMap[11] = "255.224.255.0"
	cidrMap[12] = "255.240.0.0"
	cidrMap[13] = "255.248.0.0"
	cidrMap[14] = "255.252.0.0"
	cidrMap[15] = "255.254.0.0"
	cidrMap[16] = "255.255.0.0"
	cidrMap[17] = "255.255.128.0"
	cidrMap[18] = "255.255.192.0"
	cidrMap[19] = "255.255.224.0"
	cidrMap[20] = "255.255.240.0"
	cidrMap[21] = "255.255.248.0"
	cidrMap[22] = "255.255.252.0"
	cidrMap[23] = "255.255.254.0"
	cidrMap[24] = "255.255.255.0"
	cidrMap[25] = "255.255.255.128"
	cidrMap[26] = "255.255.255.192"
	cidrMap[27] = "255.255.255.224"
	cidrMap[28] = "255.255.255.240"
	cidrMap[29] = "255.255.255.248"
	cidrMap[30] = "255.255.255.252"
	cidrMap[31] = "255.255.255.254"
	cidrMap[32] = "255.255.255.255"
}

func maskCIDRMap(cidrMap map[int]string, index int) string {
	return cidrMap[index]
}

func listIPRange(ipCDIRFormat string) []net.IP {
	_, ipv4Net, err := net.ParseCIDR(ipCDIRFormat)
	if err != nil {
		log.Fatal(err)
	}

	mask := binary.BigEndian.Uint32(ipv4Net.Mask)
	start := binary.BigEndian.Uint32(ipv4Net.IP)

	finish := (start & mask) | (mask ^ 0xffffffff)
	var numberOfIP uint32 = (finish - start) + 1
	rangeIp := make([]net.IP, numberOfIP)

	var indexOfnumberOfIP int = 0
	for i := start; i <= finish; i++ {
		ip := make(net.IP, 4)
		binary.BigEndian.PutUint32(ip, i)
		rangeIp[indexOfnumberOfIP] = ip
		indexOfnumberOfIP++
	}

	return rangeIp
}

func calculateIPAddress(ipv4 net.IP, cidr int) (net.IP, net.IP) {
	mask := net.CIDRMask(cidr, 32)
	ip := net.IP(ipv4)

	network := net.IP(make([]byte, 4))
	broadcast := net.IP(make([]byte, 4))
	for i := range ip {
		network[i] = ip[i] & mask[i]
		broadcast[i] = ip[i] | ^mask[i]
	}

	return network, broadcast
}

func main() {

	cidrMap := make(map[int]string)
	initCIDRMap(cidrMap)

	if len(os.Args) != 3 {
		fmt.Println("Help : GOSubnetCalculator <IP> <CIDR number>")
		fmt.Println("Example : GOSubnetCalculator  127.0.0.1 24")
		return
	}

	ip := os.Args[1]
	ipMask := os.Args[2]

	var ipv4 net.IP = net.ParseIP(ip)
	ipv4 = ipv4.To4()
	if ipv4 == nil {
		fmt.Println("Invalid IPV4 address")
		return
	}

	cidrIndex, err := strconv.Atoi(ipMask)
	if err != nil {
		fmt.Println(err)
		return
	}

	if cidrIndex < 8 || cidrIndex > 32 {
		fmt.Println("starting index CIDR should be between [8-32]")
		return
	}

	cidrMask := maskCIDRMap(cidrMap, cidrIndex)
	var mask net.IP = net.ParseIP(cidrMask)
	mask = mask.To4()
	if mask == nil {
		fmt.Println("Invalid IPV4 mask address")
		return
	}

	fmt.Println("Good IPV4 and IPV4 mask address.")

	var network net.IP
	var broadcast net.IP
	network, broadcast = calculateIPAddress(ipv4, cidrIndex)
	fmt.Println("For the ip address", ip)
	fmt.Println("The network is", network, "and the broadcast is", broadcast)

	ipCIDRFormat := ip + "/" + ipMask
	fmt.Println("Ip CIDR Format to pass : ", ipCIDRFormat)

	rangeIp := listIPRange(ipCIDRFormat)

	fmt.Println("Start of Ip range : ")
	for _, ip := range rangeIp {
		fmt.Println(ip)
	}
	fmt.Println("End of Ip range")
}
